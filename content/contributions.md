---
title: "Contributions"
date: 2020-07-21T09:22:13-05:00
draft: false
---

![Sudo Show Logo](/sudoshow-logo-200.png)

Eric is the creator and one of the co-hosts of the [Sudo Show Podcast](https://sudo.show), your source for all things Enterprise Open Source.

The Sudo Show is a podcast and is apart of the [Destination Linux Network](https://destinationlinux.network) and covers topics ranging from DevOps, Cloud Management, how to change your company culture and grow your people!

---

![Front Page Linux Logo](/fpl-logo-200.png)

The IT Guy is a contributor to [Front Page Linux](https://frontpagelinux.com/), a community driven destination covering open-source, technology, and Linux News! 

FPL is a site for the community created and maintained by the content creators and community members of the Destination Linux Network. FPL will feature content from your favorite podcasts, guests from across the open-source ecosystem and community members a long list of community members. The diverse set of news, opinions, guides, and solutions will help us share our passion for open-source with the DLN community and open-source enthusiasts around the world.

---

![Eric Speaker](/eric-speaker.png)

Eric is available for events, both live and virtual, meetups, and conferences. Check out his speaker page over on [SpeakerHub](https://speakerhub.com/speaker/eric-it-guy)!