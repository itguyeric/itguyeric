---
title: "About"
date: 2020-06-16T18:03:59-05:00
draft: false 
---

Fighting against the forces of burnout and poor work life balance, The IT Guy stands for DevOps, Open Source, and an endless supply of gibberish!

![Eric Profile Picture](/eric.png)

I have over 14 years of IT experience ranging from Systems Administration and Engineering to technical sales and community advocacy, most recently, as a Technical Marketing Manager at Red Hat.

My mission as the IT Guy is to fight against burnout and poor work-life balance. My goal is to promote methodologies and communities around DevOps and Open Source as well as an endless supply of fun gibberish along the way. I have worked under all the cliche'd IT cultures - from nightmare on-calls to constant firefighitng to teams that are as closed minded as the software they use. I've learned a lot from my more-than-a-decade of experience. I hope to share those stories and the lessons I learned with all of you to help make your lives and your organizations better.
