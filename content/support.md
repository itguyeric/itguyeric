---
title: "Support"
date: 2020-07-21T09:26:07-05:00
draft: false
---

Message from Eric The IT Guy:

> I have really enjoyed being a member and contributor to the Open Source Community. Not only has it bolstered my love of IT and technology, but it has allowed me a space to give back. The contributions I am making come right out of my core strengths. I am an [activator](https://www.gallup.com/cliftonstrengths/en/252140/activator-theme.aspx). I love showing people new technology or better ways of doing work. 

Head on over to Patreon to help support The IT Guy! Your contributions go towards improving the equipment and paying for hosting for audio and video production.

[![Patreon](/patreon.png)](https://www.patreon.com/itguyeric)