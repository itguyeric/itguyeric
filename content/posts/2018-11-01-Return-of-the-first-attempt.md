---
title: "Return of the First Attempt"
date: 2018-11-01T09:18:59-05:00
draft: false
toc: false
images:
tags: 
---

We got a single test episode before going back to the drawing board. After months of networking, planning, seeing what else is out there, we settled on a great approach that we feel will be unique and can make a difference in IT and how technologists do work.

https://youtu.be/055JCWt5g58
