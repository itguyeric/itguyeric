---
title: "Chillin on the Ask Noah Show"
date: 2018-10-29T17:14:00-05:00
draft: false
toc: false
images:
tags: 
---

I got to join Noah Chelliah on the Ask Noah Show to continue my coverage of Peertube. It was a great chat, took a couple of calls, and found a toy I really want from Paravel Systems: The Rivendell Audio Appliance!

https://podcast.asknoahshow.com/93
