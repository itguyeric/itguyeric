---
title: "LAS2018: Travel Day"
date: 2018-09-04T09:03:00-05:00
draft: false
toc: false
images:
tags: 
---

Kansas City, Missouri to Denver Colorado, 1 Interstate, 609 miles, 8 hours of podcasts... Nothing but rain! However, the nice thing about this road trip was it gave me time to consider a few things about my home lab, my career, you know big things.

I have spent the majority of my career deep in the weeds as a Linux Systems Engineer. I have worked with Red Hat, CentOS, and Ubuntu. The operating system has been my domain for a long time. The problem is, in the past couple of years, the area I have been deriving the most energy from has been the community. This summer, I have been working social media for the GNOME Foundation, made appearances on the Linux Unplugged podcast, helped manage Peer Tube and Gitlab instances. Now here I am on the eve of my first FOSS conference. 

Another big decision I made while dodging visibility-killing downpours was how to rebuild my home computer lab. I have been using Digital Ocean to manage services like Quassel and Nextcloud. Part of my responsibilities as a Systems Engineer is to stay on top of trends. Well, for projects to survive the velocity of development these days containers and automation need to be at the heart of all their efforts. To be a responsible Sysadmin, I need to be prepared for that shift. So, I plan on setting up a Kubernetes cluster and running all my home services out of a Gitlab instances into containers! 

The last big thought I spent time on was the next step in my education. I am a huge fan of Linux Academy. Learning from their courses is great, but it doesn't necessarily translate into something industry recognized: IE a certification. The only cert I hold at this point is a RHCSA (Red Hat Certified Systems Administrator) in RHEL6. After this road trip, I hope within the next 3 years to hold my RHCA (Red Hat Certified Architect)!

After a drink and some tacos, its time to hit the sack and get ready for LAS2018 tomorrow!