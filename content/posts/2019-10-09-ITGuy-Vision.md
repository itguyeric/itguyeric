---
title: Vision for the IT Guy
date: 2019-10-09T09:51:00-06:00
draft: false
toc: false
images:
tags: 
---

I spent over seven years in IT before I really started to understand the breadth of the industry. Technology gets a bad rap for being such a deep field but not necessarily a broad one. In fact, when I was attending college in 2009... Hold the phone... I graduated from DeVry University A DECADE ago! When did that happen!? Ugh, nothing like a personal revelation in the middle of a blog post. Any-who...the career advice I was given was there were two paths in front of me, eventually they would lead to: 1) becoming an architect, the system-designing ninja guru of a major enterprise or 2) becoming CIO of a major enterprise. Said another way, either you go the technical track or the manager track. It wasn’t ever really explained to me there were forks in those roads. Quite a few of them really! (In fact, a funny aside was that my the advisor for the first college I attended told me I wouldn’t ever be successful in IT because my higher math grades like Calculus, Trigonometry, etc. weren’t good enough. Jokes on them!)

While I am making light of a couple situations, these illustrate some very big issues in our culture, the corporate world, and our education system. (Disclaimer, I am not calling out DeVry in any sense. I loved my education and it set me up for great success. In fact, I was even crazy enough to go back and get a Masters from their graduate program.) In fact, any one person should only be limited by their own imagination or to quote the great philosopher, (Captain) Jack Sparrow: “The only rules that matter are these: what a man can do and what he can’t do.” Our rum-loving friend had a great insight. The more I learn about my career the more I realize I didn’t have a clue when I started out.

So, here’s the deal, I have been around the industry long enough to watch the shift from hardware to virtual machines to now cloud hosted workloads. I have worked within IT operations long enough to go from carrying a pager to two phones to an app-based on call rotation. I have seen the Internet go from a dial-up access to email and AIM to an essential element for virtually every industry. I have learned a thing or two along that journey and I really feel like I would be doing the industry a disservice to keep those lessons to myself. Heck, even writing that out made me think, “Hhmm, maybe I do know a couple of things!”

So, here is what I plan to bring your way. I want to address some issues that weigh on my mind: work/life balance, operational priorities, marketing buzzwords, career pathing, just to name a few. I want to share my experiences, make a few jokes, and deliver relevant news, content, and maybe a few tutorials along the way. I want to make my content available via blogs, vlogs, podcasts, conferences, whatever medium I need to use to help my fellow technologists find out where their passion and skills could best take them.

So...How about it? You ready?
