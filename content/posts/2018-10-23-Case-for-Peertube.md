---
title: "The Case for Peertube"
date: 2018-10-23T09:03:00-05:00
draft: false
toc: false
images:
tags: 
---

I had the pleasure to head out to Lawrence, KS to talk at the local LUG (Linux Users Group)! Today's topic? Peertube and the problems it can solve!

https://youtu.be/IsRMSVIZnlo
