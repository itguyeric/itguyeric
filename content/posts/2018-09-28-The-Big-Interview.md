---
title: "The Big Interview!"
date: 2018-09-28T09:17:00-05:00
draft: false
toc: false
images:
tags: 
---

Sometimes, a dream just comes together. Sometimes its a dream that you didn't even know you had!

I have spent 10 years in the IT industry and I have struggled off and on to really find fulfillment in what I have accomplished thus far, at least until the past 4 months. If you know me at all, you know that I hit the ground running when I entered the open source community. When I started listening to podcasts over 2 years ago, I was an Apple die-hard: Macbook, iPad, iPhone, AppleTV, Apple Watch, iTunes...Yeah, it was bad! What I found through listening to Jupiter Broadcasting though was a passionate community of people willing to work hard to create something of their own.

Over the past two years, I have listened intentely, idled in Telegram, and even provided a small donation every month as a Patron. It wasn't until earlier this year that I really started to find my stride on how I fit into this mess. Enter the "IT Guy". The online identity came about because when joining the Mumble room for Linux Unplugged, there were two Eric's: myself and Erich, a respected member of the Ubuntu Studio project. That can be confusing during a live show or even just during chat. As the occasional IT consultant, I tended to hear, "Hey, the IT guy is here..." and the name just stuck! Moving forward, I hope the IT Guy will be able to be a platform for open source projects who need a hand getting some attention. I am working with an open-source graphic design company, FreeHive, to create some branding around the identity and start producing more blog entries, a podcast, and to even speak at conferences one day.

Over the past 4 months, that vision has really started to take shape! Along came Peertube, an open-source project working to create a federated video platform powered by torrent technology (think a self-hosted YouTube). I volunteered and started helping Jupiter Broadcasting to setup GetJupiter.com, a peertube instance focused on official Jupiter Broadcasting content and Linux Rocks Online (a community driven instance). I have been hanging out on Linux Unplugged more often and even been a guest on the Ask Noah show a couple of times...

But NONE of that compares to what I experienced today. Today, I was flown out to Dallas, TX to complete the interview process with Red Hat! Yeah...that $2.4 billion company that sells free software. Talk about an inspiration. This company is filled to the brim with talent, passion, and creativity. While I can't say much about the process itself, it has been unlike any interview process I have ever been apart of. You talk about a group of people focused on their mission!

It'll be a few days before I hear for sure, but I am very hopeful! My new role would be as a Senior Cloud Infrastructure Consultant. My new best friend would be Open Shift, Red Hat's offering built around Kubernetes! For me, it would be more than setting up Open Shift for customers, it would be a chance to provide customers, specifically other Systems Engineers like myself, a new way to do work: focus on automation and orchestration instead of fighting through the same fires over and over again. Get to the point where deployments during business hours are common place and reclaim a work-life balance, recover their nights and weekends. That is a role I could make a difference in.

I hope my next entry will be to announce a shift in my employment, but regardless of what happens with Red Hat, I will continue to work to build a platform where open source projects can come be heard and provide great information and insight into the IT world!

Thank you for reading!