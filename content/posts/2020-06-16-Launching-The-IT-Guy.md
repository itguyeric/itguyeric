---
title: Launching the IT Guy
date: 2020-06-16T09:51:00-06:00
draft: false
toc: false
images:
tags: 
---

{{< youtube vpr8mUOnhYI >}}

I have some very exciting news to share with you all!

My name is Eric The IT Guy and I am a recovering Systems Administrator! 

I have over 11 years of IT experience ranging from Systems Administration and Engineering to technical sales and community advocacy, most recently, as a Sales Solutions Architect at Red Hat.

My mission as the IT Guy is to fight against burnout and poor work life balance. My goal is to promote methodologies and communities around DevOps and Open Source as well asn an endless supply of fun gibberish along the way.

I have worked under all the cliche'd IT cultures - from nightmare on-calls to constant firefighitng to teams that are as closed minded as the software they use. I've learned a lot from my more-than-a-decade of experience. I hope to share those stories and the lessons I learned with all of you to help make your lives and your organizations better.

Now with that said, here's the news: I am very excited to announce that I have joined the [Destination Linux Network](https://destinationlinux.network)! If you've not heard of DLN . . . where have you been? It's okay, I'll just tell you.

The Destination Linux Network is a media network powered by Linux and Open Source with a focus on bringing quality content to our audience to help you learn and enjoy the awesome technologies that we all have available.

On DLN, I'll be one of the hosts of the [Sudo Show](https://sudo.show) podcast which will be your place for all things enterprise open source. As I said, I'll be ONE of the hosts and joining me is Brandon Johnson, a fellow Red Hatter . . . oh did I mention I work at Red Hat?

The Sudo Show is going to be an awesome podcast covering careers in IT, productivity, and enterprise technology. If you are just getting started or a seasoned veteran looking to "keep up" then you will certainly want to subscribe to the show! You can get our content on the [DLN YouTube Channel](https://www.youtube.com/channel/UCWJUSpXVHTaHErtGWC5qPlQ) or subscribe to the audio version wherever you get your podcasts.

In addition to the Sudo Show, I'm joining the team at [Front Page Linux](https://frontpagelinux.com/). I will be writing articles at Front Page Linux Dot Com; these will cover topics like avoiding burnout, ways to boost productivity, and how to impact your companies' culture for the better.

Last but not least, we get to my [Youtube Channel](https://www.youtube.com/channel/UCBH28iYTnBxoO7Gl1A3mO1g). I will be releasing periodic Vlogs there for more random content like an inside look at my experiences as a Solutions Architect.

I am excited to be joining such an awesome community. If you'd like to get in touch, just shoot an email over to [Contact At Sudo Dot Show](mailto:contact@sudo.show). You can follow me At IT Guy Eric on [Facebook](https://www.facebook.com/itguyeric), [Twitter](https://twitter.com/itguyeric), [LinkedIn](https://www.linkedin.com/in/itguyeric/), and [Mastodon Dot Social](https://mastodon.social/web/accounts/1183764)...just to name a few.

I can't wait to get started, our first release is June 25th. I look forward to sharing my experiences with you and especially getting your feedback!
